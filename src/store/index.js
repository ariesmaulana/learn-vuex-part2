import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import product from './product';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    product,
  },
  state: {
    name: 'John Doe',
    info: 'global info',
    eMsg: '',
    users: [],
  },
  getters: {
    getName(state) {
      return `Selamat Datang: ${state.name}`;
    },
    getUsers(state) {
      return state.users;
    },
    getInfo(state) {
      return state.info;
    },
  },
  mutations: {
    setName(state, payload) {
      // eslint-disable-next-line no-param-reassign
      state.name = payload;
    },
    setUsers(state, payload) {
      // eslint-disable-next-line no-param-reassign
      state.users = payload;
    },
    setErrorMessage(state, payload) {
      // eslint-disable-next-line no-param-reassign
      state.eMsg = payload;
    },
    setInfo(state, payload) {
      // eslint-disable-next-line no-param-reassign
      state.info = payload;
    },
  },
  actions: {
    async fetchUserList({ commit }) {
      await axios.get('http://jsonplaceholder.typicode.com/users')
        .then((res) => {
          commit('setUsers', res.data);
        })
        .catch((e) => {
          commit('setErrorMessage', e.message);
        });
    },
  },
});
