export default {
  namespaced: true,
  state: {
    info: 'product info',
    products: [],
  },
  getters: {
    getInfo(state) {
      return state.info;
    },
    getProduct(state) {
      return state.products;
    },
    // eslint-disable-next-line
    getProductByIndex: state => (index) => {
      return state.products[index];
    },
  },
  mutations: {
    setNewProduct(state, payload) {
      // eslint-disable-next-line no-param-reassign
      state.products.push(payload);
    },
    setUpdateProduct(state, payload) {
      // eslint-disable-next-line prefer-destructuring
      const index = payload.index;
      // eslint-disable-next-line no-param-reassign
      state.products[index].title = payload.title;
      // eslint-disable-next-line no-param-reassign
      state.products[index].description = payload.description;
    },
    setRemoveProduct(state, index) {
      state.products.splice(index, 1);
    },
  },
  actions: {
    addProduct({ commit }, payload) {
      commit('setNewProduct', payload);
      commit('setInfo', 'Produk ditambahkan', { root: true });
    },
    updateProduct({ commit }, payload) {
      commit('setUpdateProduct', payload);
      commit('setInfo', 'Produk diubah', { root: true });
    },
    deleteProduct({ commit }, payload) {
      commit('setRemoveProduct', payload);
      commit('setInfo', 'Produk dihapus', { root: true });
    },
  },

};
